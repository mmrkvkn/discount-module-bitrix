<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Application;
use Bitrix\Main\IO\Directory;

class Mmrkvkn_Discount extends CModule
{

    var $MODULE_ID = "mmrkvkn.discount";
    var $errors;

    public function __construct()
    {
        if (is_file(__DIR__ . '/version.php')) {
            include_once(__DIR__ . '/version.php');
            $this->MODULE_VERSION      = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
            $this->MODULE_NAME         = 'Скидки';
            $this->MODULE_DESCRIPTION  = 'Работа со скидками';
        } else {
            Loc::getMessage('Файл не найден: ') . ' version.php';
        }
    }

    /**
     * Метод установки модуля
     * 
     * @return void
     */
    public function doInstall()
    {
        $this->InstallDB();
        ModuleManager::RegisterModule($this->MODULE_ID);
    }

    /**
     * Метод удаления модуля
     * 
     * @return void
     */
    public function doUninstall(): void
    {
        $this->UnInstallDB();
        ModuleManager::UnRegisterModule($this->MODULE_ID);
    }

    /**
     * Создание таблицы
     * 
     * @return array|bool 
     */
    function InstallDB(): array|bool
    {
        global $DB;

        $this->errors = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT'] . "/local/modules/mmrkvkn.discount/install/db/install.sql");
        if (!$this->errors) {
            return true;
        } else {
            return $this->errors;
        }
    }

    /**
     * Удаление таблицы
     * 
     * @return array|bool
     */
    function UnInstallDB(): array|bool
    {
        global $DB;
        $this->errors = false;
        $this->errors = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT'] . "/local/modules/mmrkvkn.discount/install/db/uninstall.sql");
        if (!$this->errors) {
            return true;
        } else
            return $this->errors;
    }

    /**
     * Установка компонента
     * 
     * @return void
     */
    public function InstallFiles(): void
    {
        CopyDirFiles(
            __DIR__ . "/componetns/mmrkvkn.discount",
            Application::getDocumentRoot() . "/local/componetns/" . $this->MODULE_ID,
            true,
            true
        );
    }

    /**
     * Удаление компнента
     * 
     * @return void
     */
    public function UnInstallFiles(): void
    {
        Directory::deleteDirectory(
            Application::getDocumentRoot() . "/local/componetns/" . $this->MODULE_ID . '/'
        );
    }
}
