<?php
/*
 * Файл local/modules/scrollup/install/version.php
 */

$arModuleVersion = [
    'VERSION'      => '1.0.0',
    'VERSION_DATE' => '2023-04-29 20:00'
];