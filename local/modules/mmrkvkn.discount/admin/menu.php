<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Mmrkvkn\Discount\Discounts\AdminInterface\DiscountsListHelper;


if (!Loader::includeModule('digitalwand.admin_helper') || !Loader::includeModule('mmrkvkn.discount')) return;

Loc::loadMessages(__FILE__);

return [
    [
        'parent_menu' => 'global_menu_content',
        'sort' => 1,
        'icon' => 'sale_menu_icon_statistic',
        'page_icon' => 'sale_menu_icon_statistic',
        'text' => 'Скидки',
        'url' => DiscountsListHelper::getUrl()
    ]
];
