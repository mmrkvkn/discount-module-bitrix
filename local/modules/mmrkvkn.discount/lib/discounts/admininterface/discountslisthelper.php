<?php

namespace Mmrkvkn\Discount\Discounts\AdminInterface;

use DigitalWand\AdminHelper\Helper\AdminListHelper;

/**
 * Хелпер описывает интерфейс, выводящий список скидок.
 *
 * {@inheritdoc}
 */
class DiscountsListHelper extends AdminListHelper
{
	protected static $model = '\Mmrkvkn\Discount\Discounts\DiscountsTable';
}