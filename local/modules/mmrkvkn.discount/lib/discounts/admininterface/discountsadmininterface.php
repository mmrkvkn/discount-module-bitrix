<?php

namespace Mmrkvkn\Discount\Discounts\AdminInterface;

use DigitalWand\AdminHelper\Helper\AdminInterface;
use DigitalWand\AdminHelper\Widget\DateTimeWidget;
use DigitalWand\AdminHelper\Widget\NumberWidget;
use DigitalWand\AdminHelper\Widget\StringWidget;
use DigitalWand\AdminHelper\Widget\UserWidget;

/**
 * Описание интерфейса (табок и полей) админки новостей.
 *
 * {@inheritdoc}
 */
class DiscountsAdminInterface extends AdminInterface
{
    /**
     * @inheritdoc
     */
    public function dependencies()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        return [
            'MAIN' => [
                'NAME' => 'Скидка',
                'FIELDS' => [
                    'ID' => [
                        'WIDGET' => new NumberWidget(),
                        'READONLY' => true,
                        'FILTER' => true,
                        'HIDE_WHEN_CREATE' => true,
                        'TITLE' => 'ID'
                    ],
                    'CODE' => [
                        'WIDGET' => new StringWidget(),
                        'SIZE' => 50,
                        'FILTER' => '%',
                        'REQUIRED' => true,
                        'TITLE' => 'Код скидки'
                    ],
                    'PROCENT' => [
                        'WIDGET' => new NumberWidget(),
                        'FILTER' => true,
                        'TITLE' => 'Процент скидки'
                    ],
                    'CREATED' => [
                        'WIDGET' => new DateTimeWidget(),
                        'READONLY' => true,
                        'HIDE_WHEN_CREATE' => true,
                        'TITLE' => 'Дата создания скидки'
                    ],
                    'USER' => [
                        'WIDGET' => new UserWidget(),
                        'TITLE' => 'Пользователь'
                    ]
                ]
            ]
        ];
    }


    /**
     * Подлюкчение всех хэлперов, для отображние деталки и списка скидок
     * 
     * @return array
     */
    public function helpers(): array
    {
        return [
            '\Mmrkvkn\Discount\Discounts\AdminInterface\DiscountsListHelper' => [
                'BUTTONS' => [
                    'LIST_CREATE_NEW' => [
                        'TEXT' => 'Добавить скидку',
                    ]
                ]
            ],
            '\Mmrkvkn\Discount\Discounts\AdminInterface\DiscountsEditHelper' => [
                'BUTTONS' => [
                    'ADD_ELEMENT' => [
                        'TEXT' => 'Добавить скидку'
                    ],
                    'DELETE_ELEMENT' => [
                        'TEXT' => 'Удалить скидку'
                    ]
                ]
            ]
        ];
    }
}
