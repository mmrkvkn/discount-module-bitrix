<?php

namespace Mmrkvkn\Discount\Discounts\AdminInterface;

use DigitalWand\AdminHelper\Helper\AdminEditHelper;

/**
 * Хелпер описывает интерфейс, выводящий форму редактирования скидки.
 *
 * {@inheritdoc}
 */
class DiscountsEditHelper extends AdminEditHelper
{
    protected static $model = '\Mmrkvkn\Discount\Discounts\DiscountsTable';
}
