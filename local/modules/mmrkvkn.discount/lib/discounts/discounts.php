<?php

namespace Mmrkvkn\Discount\Discounts;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type\DateTime;

Loc::loadMessages(__FILE__);

/**
 * Модель скидок.
 */
class DiscountsTable extends DataManager
{
    /**
     * @inheritdoc
     */
    public static function getTableName()
    {
        return 'users_discounts';
    }

    /**
     * @inheritdoc
     */
    public static function getMap()
    {
        return [
            'ID' => [
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true
            ],
            'CODE' => [
                'data_type' => 'string',
                'title' => 'CODE'
            ],
            'PROCENT' => [
                'data_type' => 'integer',
                'default_value' => 0
            ],
            'CREATED' => [
                'data_type' => 'datetime',
                'title' => 'DATE',
                'default_value' => new DateTime()
            ],
            'USER' => [
                'data_type' => 'integer',
                'title' => 'USER',
                'default_value' => static::getUserId()
            ],
        ];
    }

    /**
     * Возвращает идентификатор пользователя.
     *
     * @return int|null
     */
    public static function getUserId(): int|null
    {
        global $USER;

        return $USER->GetID();
    }
}
