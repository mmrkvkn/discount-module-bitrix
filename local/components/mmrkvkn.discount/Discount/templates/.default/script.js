const getSales = () => {
  BX.ajax.runComponentAction('mmrkvkn.discount:Discount', 'getDiscount', {
    mode: 'class',
    data: {},

  }).then(function (response) {
    const inputValue = document.getElementById("inputValue");
    inputValue.value = response.data.CODE;
  });
}

const checkCoupon = () => {
  const inputValue = document.getElementById("inputValue").value;
  BX.ajax.runComponentAction('mmrkvkn.discount:Discount', 'checkCoupon', {
    mode: 'class',
    data: {
      'code': inputValue
    },
  }).then(function (response) {
    if (response.data !== null) {
      document.getElementById("myBlock").innerHTML = `Скидка: ${response.data} %`;
    } else {
      document.getElementById("myBlock").innerHTML = 'Скидка недоступна';
    }
  });
}

