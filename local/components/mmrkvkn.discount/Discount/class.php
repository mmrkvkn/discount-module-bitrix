<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Loader;
use Bitrix\Main\Type\Date;
use Bitrix\Main\Type\DateTime;
use Mmrkvkn\Discount\Discounts\DiscountsTable;

class Discount extends CBitrixComponent implements \Bitrix\Main\Engine\Contract\Controllerable
{
    /**
     * @var array Массив для генерации кода купона
     */
    protected array $couponsSymbols = [
        'a', 'b', 'c', 'd', 'e', 'f',
        'g', 'h', 'i', 'j', 'k', 'l',
        'm', 'n', 'o', 'p', 'r', 's',
        't', 'u', 'v', 'x', 'y', 'z',
        'A', 'B', 'C', 'D', 'E', 'F',
        'G', 'H', 'I', 'J', 'K', 'L',
        'M', 'N', 'O', 'P', 'R', 'S',
        'T', 'U', 'V', 'X', 'Y', 'Z',
        '1', '2', '3', '4', '5', '6',
        '7', '8', '9', '0'
    ];

    /**
     * @var int Минимальная скидка
     */
    protected int $minDiscount = 1;

    /**
     * @var int Максимальная скидка
     */
    protected int $maxDiscount = 50;

    /**
     * @var int Длина кода для скидки 
     */
    protected int $couponLength = 6;

    /**
     * Выполнение компонента
     * 
     * @return void
     */
    public function executeComponent(): void
    {
        global $USER;
        if (!Loader::includeModule("mmrkvkn.discount")) {
            return;
        }
        if (!$USER->isAuthorized()) {
            LocalRedirect('/');
        }
        $this->includeComponentTemplate();
    }

    /**
     * Конфигурация экшенов компонента
     * 
     * @return array
     */
    public function configureActions(): array
    {
        return [];
    }

    /**
     * Метод для получения кода и скидки пользователю
     * 
     * @return array
     */
    public function getDiscountAction(): array
    {
        Loader::includeModule("mmrkvkn.discount");
        $coupon = [];
        $usersCoupon = $this->getUsersCoupon();
        /* Если есть купон часовой давности */
        if ($usersCoupon?->getCreated()->add('+1 hour') > new DateTime()) {
            $coupon = [
                'CODE' => $usersCoupon->getCode(),
                'PROCENT' => $usersCoupon->getProcent()
            ];
        } else {
            $coupon = $this->getNewCoupon();
        }

        return $coupon;
    }


    /**
     * Генерация нового купона
     * 
     * @return array
     */
    private function getNewCoupon(): array
    {
        global $USER;
        $procent = $this->getRandomDiscount();
        $user = $USER->getId();
        $code = $this->generateCoupon();

        /* Проверяем, есть такой код у нас в таблице, если есть, то генерируем новый  */
        while ($this->isCouponExist($code)) {
            $code = $this->generateCoupon();
        }
        DiscountsTable::add([
            'CODE' => $code,
            'PROCENT' => $procent,
            'USER' => $user
        ]);
        return [
            'CODE' => $code,
            'PROCENT' => $procent
        ];
    }

    /**
     * Создаёт случайный размер скидки
     * 
     * @return int
     */
    private function getRandomDiscount(): int
    {
        return rand($this->minDiscount, $this->maxDiscount);
    }

    /**
     * Генерация кода для купона
     * 
     * @return string
     */
    private function generateCoupon(): string
    {
        $code = "";
        for ($i = 0; $i < $this->couponLength; $i++) {
            $index = rand(0, count($this->couponsSymbols) - 1);
            $code .= $this->couponsSymbols[$index];
        }
        return $code;
    }

    /**
     * Проверка купона на существование
     * 
     * @param string $code
     * 
     * @return bool
     */
    private function isCouponExist(string $code): bool
    {
        $isCouponExist = false;
        $coupon = DiscountsTable::getList([
            'filter' => [
                'CODE' => $code
            ]
        ])->fetchObject();

        ($coupon) ? $isCouponExist = true : $isCouponExist = false;

        return $isCouponExist;
    }

    /**
     * Проверяет, есть ли у пользователя купон, созданный ранее одного часа
     * 
     * @return bool
     */
    private function getUsersCoupon(): object|null
    {
        global $USER;
        $discount = DiscountsTable::getList([
            'filter' => [
                'USER' => $USER->getId()
            ],
            'order' => [
                'CREATED' => 'DESC'
            ],
            'limit' => 1,
        ])->fetchObject();

        return $discount;
    }

    /**
     * Проверяет купон пользователя
     * @param string $code
     * 
     * @return int
     */
    public function checkCouponAction(string $code): int|null
    {
        Loader::includeModule("mmrkvkn.discount");
        global $USER;
        $procent = DiscountsTable::getList([
            'filter' => [
                'USER' => $USER->getId(),
                '>CREATED' => (new DateTime())->add('-3 hour'),
                '=CODE' => $code
            ],
            'order' => [
                'CREATED' => 'DESC'
            ],
            'limit' => 1
        ])->fetchObject()?->getProcent();

        return $procent;
    }
}
