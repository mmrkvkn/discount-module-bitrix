Реализация простого модуля для работы с генерацией скидок

Разработан на php 8
Логика административной части сделана через модуль digitalwand.admin_helper

В начале нужно установить 2 модуля:
mmrkvkn.discount и digitalwand.admin_helper

mmrkvkn.discount установит нужную таблицу, для работы со скидками (сделано отдельной таблицей, НЕ ИНФОБЛОКОМ)

Далее нужно, под авторизованным пользователем зайти на страницу discounts

Здесь будет форма реализованная через компонент mmrkvkn.discount:Discoutns
Папки где лежит основаная логика:

/local/components/mmrkvkn.discount/Discount
/home/mmrkvkn/projects/php-projects/officeMag/www/local/modules/mmrkvkn.discount

Скидки можно просмотреть на вкладке "Контент" => "Скидки"(За генерацию страницы отвечает digitalwand.admin_helper):

bitrix/admin/admin_helper_route.php?lang=ru&module=mmrkvkn.discount&view=discounts_list&entity=discounts
